//var socket = io.connect('http://192.168.1.20:8080', {'forceNew': true});
var socket = io.connect('http://localhost:6321', {'forceNew': true});
//var socket = io.connect('http://dedalo.duckdns.org:6321', {'forceNew': true});

socket.on('messages', function(data) {
  console.log(data);
  render(data);
})

function render(data){

  var html = data.map(function(elem, index){
    return(`<div>
                      <strong>${elem.author}</strong>:
                      <em>${elem.text}</em>
                </div>`);
  }).join(" ");

  document.getElementById('messages').innerHTML = html;
}

function addMessage(e){
  var payload= {
    author: document.getElementById('username').value,
    text: document.getElementById('texto').value
  };

  socket.emit('newMessage', payload);
  return false;
}
