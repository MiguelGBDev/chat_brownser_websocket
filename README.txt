AUTOR:
Miguel Angel Gomez Baena

Este proyecto simple contiene un chat en tiempo real a traves de navegador.
Utilizando la tecnologia nodejs y javascript, somos capaces de modificar el cuerpo HTML sin tener que recargar la pagina.

-El protocolo utilizando para la comunicacion es WebSocket.
-Utilizamos la API SOCKET.IO para trabajar a alto nivel sobre los WebSockets.
-Contiene cuerpo HTML, ficheros JAVASCRIPT y Servidor corriendo en la tecnologia NODEJS bajo el puerto 6321.

El proyecto es creado como muestra de implementacion basica de estas tecnologias.
