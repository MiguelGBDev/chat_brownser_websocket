var express = require('express');
var app = express();
var server = require('http').Server(app);

var io = require('socket.io')(server);

//var http = require('http');

//no es escalable dice el tio
var messages = [{
    id:1,
    text:"hola soy un mensaje",
    author:"yo"
}]

app.use(express.static('../public'));

app.get('/hello', function(req, res){
  res.status(200).send("hola mundo");
});

io.on('connection', function(socket){
  console.log('alguien se ha conectado por sockets');
  socket.emit('messages', messages);

  socket.on('newMessage', function(data){
    messages.push(data);

    io.sockets.emit('messages', messages);
  });
});


server.listen(6321, function() {
  console.log("conectado y tal");
});

/*
// Load the http module to create an http server.
var http = require('http');

// Configure our HTTP server to respond with Hello World to all requests.
var server = http.createServer(function (request, response) {
  response.writeHead(200, {"Content-Type": "text/plain"});
  response.end("Hello World\n");
});
*/
